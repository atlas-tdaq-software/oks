/**
 *  \file oks_dump.cpp
 *
 *  This file is part of the OKS package.
 *  Author: <Igor.Soloviev@cern.ch>
 *
 *  This file contains the implementation of the OKS application to dump
 *  contents of the OKS database files.
 *
 */

#include <vector>
#include <iostream>

#include <string.h>
#include <stdlib.h>

#include <oks/relationship.h>
#include <oks/kernel.h>
#include <oks/exceptions.h>


enum __OksDumpExitStatus__ {
  __Success__ = 0,
  __BadCommandLine__,
  __ExceptionCaught__
};

int
main(int argc, char **argv)
{
  if(argc < 3) {
    std::cerr << "provide \"data file name\", \"test number\" and optional parameter\n";
    return __BadCommandLine__;
  }

  try {
      OksKernel k;
      auto f = k.load_file(argv[1]);
      auto i = atoi(argv[2]);
      const char * param = (argc > 3 ? argv[3] : nullptr);
      const char * param2 = (argc > 4 ? argv[4] : nullptr);
      const char * param3 = (argc > 5 ? argv[5] : nullptr);
      const char * param4 = (argc > 6 ? argv[6] : nullptr);
      const char * param5 = (argc > 7 ? argv[7] : nullptr);

      if (i == 1)
        {
          if (argc != 4)
            {
              std::cerr << "provide \"data file name\", \"test number\" and optional parameter\n";
              return __BadCommandLine__;
            }

          std::cout << "[TEST]: add include \"" << param << "\"\n";
          f->add_include_file(param);
        }
      else if (i == 2)
        {
          if (!f->get_include_files().empty())
            {
              auto include = *f->get_include_files().begin();
              std::cout << "[TEST]: remove include \"" << include << "\"\n";
              f->remove_include_file(include);
            }
        }
      else if (i == 3)
        {
          if (!f->get_include_files().empty())
            {
              auto old_include = *f->get_include_files().begin();
              std::cout << "[TEST]: rename include \"" << old_include << "\" to \"" << param << "\"\n";
              f->remove_include_file(old_include);
              f->add_include_file(param);
            }
        }
      else if (i == 11)
        {
          if (argc != 5)
            {
              std::cerr << "provide \"data file name\", \"test number\" and two parameters: author and text\n";
              return __BadCommandLine__;
            }

          std::cout << "[TEST]: add comment author=\"" << param << "\" and text=\"" << param2 << "\"\n";

          f->add_comment(param2, param);
        }
      else if (i == 12)
        {
          if (argc != 4)
            {
              std::cerr << "provide \"data file name\", \"test number\" and one parameters: number of comments to be removed\n";
              return __BadCommandLine__;
            }

          auto n = atoi(param);

          while (n-- > 0 && !f->get_comments().empty())
            {
              const std::string ts = f->get_comments().begin()->first;
              std::cout << "[TEST]: remove comment ts=\"" << ts << "\"\n";
              f->remove_comment(ts);
            }
        }
      else if (i == 13)
        {
          if (argc != 5)
            {
              std::cerr << "provide \"data file name\", \"test number\" and two parameters: author and text\n";
              return __BadCommandLine__;
            }

          if (!f->get_comments().empty())
            {
              const std::string ts = f->get_comments().begin()->first;
              std::cout << "[TEST]: modify comment at \"" << ts << "\" author=\"" << param << "\" and text=\"" << param2 << "\"\n";
              f->modify_comment(ts, param2, param);
            }
        }
      else if (i == 21)
        {
          // 21 class obj attribute new value
          if (argc != 7)
            {
              std::cerr << "update attribute: provide \"data file name\", \"test number\", \"class-name\", \"object-id\", \"attribute-name\", \"value\"\n";
              return __BadCommandLine__;
            }

          if (OksClass* c = k.find_class(param))
            {
              if (OksObject * o = c->get_object(param2))
                {
                  if (OksAttribute * a = c->find_attribute(param3))
                    {
                      OksData d;
                      d.SetValues(param4, a);
                      o->SetAttributeValue(param3, &d);
                    }
                  else
                    {
                      throw std::runtime_error(std::string("Cannot find attribute: ") + param3 + " in class " + param);
                    }
                }
              else
                {
                  throw std::runtime_error(std::string("Cannot find object: ") + param2 + "@" + param);
                }
            }
          else
            {
              throw std::runtime_error(std::string("Cannot find class: ") + param);
            }
        }
      else if (i == 23)
        {
          // 23 class obj attribute new value
          if (argc < 5)
            {
              std::cerr << "new object: provide \"data file name\", \"test number\", \"class-name\", \"object-id1\", [\"object-id1\"*]\n";
              return __BadCommandLine__;
            }

          if (OksClass* c = k.find_class(param))
            {
              k.set_active_data(f);

              for (int idx = 4; idx < argc; ++idx)
                new OksObject(c, argv[idx]);
            }
          else
            {
              throw std::runtime_error(std::string("Cannot find class: ") + param);
            }
        }
      else if (i == 31)
        {
          // 21 class obj attribute new value
          if (argc != 8)
            {
              std::cerr << "update relationship: provide \"data file name\", \"test number\", \"class-name\", \"object-id\", \"attribute-name\", \"class\", \"id\"n";
              return __BadCommandLine__;
            }

          if (OksClass* c = k.find_class(param))
            {
              if (OksObject * o = c->get_object(param2))
                {
                  if (OksRelationship * r = c->find_relationship(param3))
                    {
                      if (OksClass * c2 = k.find_class(param4))
                        {
                          OksData d;

                          if (OksObject * o2 = c2->get_object(param5))
                            d.Set(o2);
                          else
                            d.Set(c2, param5);

                          if (r->get_high_cardinality_constraint() == OksRelationship::Many)
                            {
                              OksData d2(new OksData::List());
                              d2.data.LIST->push_back(&d);
                              o->SetRelationshipValue(param3, &d2);
                              d2.data.LIST->clear(); // avoid delete on "&d"
                            }
                          else
                            {
                              o->SetRelationshipValue(param3, &d);
                            }
                        }
                      else
                        {
                          throw std::runtime_error(std::string("Cannot find class: ") + param4);
                        }
                    }
                  else
                    {
                      throw std::runtime_error(std::string("Cannot find relationship: ") + param3 + " in class " + param);
                    }
                }
              else
                {
                  throw std::runtime_error(std::string("Cannot find object: ") + param2 + "@" + param);
                }
            }
          else
            {
              throw std::runtime_error(std::string("Cannot find class: ") + param);
            }
        }
      k.update_data(f);
  }
  catch (oks::exception & ex) {
    std::cerr << "Caught oks exception:\n" << ex << std::endl;
    return __ExceptionCaught__;
  }
  catch (std::exception & e) {
    std::cerr << "Caught standard C++ exception: " << e.what() << std::endl;
    return __ExceptionCaught__;
  }
  catch (...) {
    std::cerr << "Caught unknown exception" << std::endl;
    return __ExceptionCaught__;
  }

  return __Success__;
}
